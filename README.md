# Desafio 4 - Agriness - Desenvolvimento

## Introdução

Bem-vindo ao Desafio Agriness.

Esse desafio tem apenas o objetivo de entender como você aplicaria alguns conceitos técnicos que atualmente são utilizados por nós.

A seguir descrevemos os requisito do desafio que você poderá desenvolver utilizando algumas das tecnologias especificada na vaga (consultar descrição da vaga).

## Desafio

Desejamos criar um buscador de oportunidades de trabalho baseado no Github Jobs (1). Este buscador deverá ter no mínimo os seguintes requisitos:

* Deverá ser possível realizar buscas por localização;
* Deverá ser possível realizar buscas pela descrição da vaga;
* Deverá ser possível realizar filtros por trabalhos "full time";
* Na listagem dos resultado da busca deverão ser exibidos: título, empresa (com logo), localização, tipo e data de criação;
* No detalhe de uma vaga deverão ser exibidas: descrição, como aplicar, url da vaga no Github Job, além das informações presentes na listagem;
* O sistema deverá armazenar temporariamente (local storage) pesquisas marcadas como favoritas;

## Observações:

- Implemente testes para garantir a qualidade da entrega.
- Documente o projeto para que outros desenvolvedores possam executá-lo facilmente.
- Ao concluir compartilhe conosco o repositório GIT (Bitbucket ou GitHub). Certifique-se que teremos acesso total a ele.
- Sinta-se a vontade para definir o layout.

Referências:

(1) [Documentação da API do GitHub Jobs](https://jobs.github.com/api)